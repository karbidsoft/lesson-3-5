import requests
from urllib.parse import urlencode, urlparse, urljoin

# AUTH_URL = 'https://oauth.yandex.ru/authorize'
TOKEN = 'AQAAAAAMZLZHAAQxP-qquNTmDkZtrjZO-9Qc4Hw'
# APP_ID = '37cbd9e48fd64bddad3df79fb54c07d5'


class YandexMetrika(object):
    STAT_URL = 'https://api-metrika.yandex.ru/stat/v1/'
    token = None

    def __init__(self, token):
        self.token = token

    def http_header(self):
        headers = {
            'Content-Type': 'application/x-yametrika+json',
            'Authorization': 'OAuth {}'.format(self.token),
            'User-Agent': 'python3'
        }
        return headers

    def visitors_list(self, counter_id):
        url = urljoin(self.STAT_URL, 'data')
        headers = self.http_header()
        params = {
            'id': counter_id,
            'metrics': 'ym:s:users'
        }
        response = requests.get(url, params, headers=headers)
        result = response.json()['data'][0]['metrics'][0]
        return result

    def views_list(self, counter_id):
        url = urljoin(self.STAT_URL, 'data')
        headers = self.http_header()
        params = {
            'id': counter_id,
            'metrics': 'ym:s:pageviews'
        }
        response = requests.get(url, params, headers=headers)
        result = response.json()['data'][0]['metrics'][0]
        return result

    def visits_list(self, counter_id):
        url = urljoin(self.STAT_URL, 'data')
        headers = self.http_header()
        params = {
            'id': counter_id,
            'metrics': 'ym:s:visits'
        }
        response = requests.get(url, params, headers=headers)
        result = response.json()['data'][0]['metrics'][0]
        return result


metrika = YandexMetrika(TOKEN)
result = metrika.visitors_list('44061454')
print('Уникальных посетителей: ', result)
result = metrika.views_list('44061454')
print('Уникальных просмотров: ', result)
result = metrika.visits_list('44061454')
print('Уникальных визитов: ', result)
